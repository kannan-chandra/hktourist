function distance(lat1,long1,lat2,long2) {
	var kmDist = KMFromLatlong(lat1-lat2,long1-long2);
	return Math.sqrt((kmDist.latKM * kmDist.latKM) + (kmDist.longKM * kmDist.longKM));
}

//sorts a list of locations (which have lat and long) by dist from a reference lat and long
function sortByDistance(list,originLat,originLong) {
	return _.sortBy(list,function(item){
		// var coord = latlongFromGeocode(item.easting,item.northing);
		return distance(originLat,originLong,item.latitude,item.longitude); 
	});
}


function latlongFromGeocode(easting,northing) {

	// constants
	var NorthPerLat = 103182.40621;
	var EastPerLong = 111205.78011;
	var PrecLat = 22.252403;
	var PrecLong =  114.134579;
	var PrecEast = 831890;
	var PrecNorth = 812621;

	var latitude;
	var longitude;

	var eastdiff = easting - PrecEast;
	var northdiff = northing - PrecNorth;

	latitude = (northdiff/NorthPerLat) + PrecLat;
	longitude = (eastdiff/EastPerLong) + PrecLong;

	return {
		latitude:latitude,
		longitude:longitude
	};
}


function KMFromLatlong(latitude,longitude) {
	var KM_PER_LONG = 106.30;
	var KM_PER_LAT = 111;
	
	return {
		latKM: latitude * KM_PER_LAT,
		longKM: longitude * KM_PER_LONG
	};
}


function getCurrentLocation(callback) {
	Ti.Geolocation.preferredProvider = "gps";
	
	// if (isIPhone3_2_Plus())
	// {
		// //NOTE: starting in 3.2+, you'll need to set the applications
		// //purpose property for using Location services on iPhone
		// Ti.Geolocation.purpose = "GPS demo";
	// }
// 	
	function translateErrorCode(code) {
		if (code == null) {
			return null;
		}
		switch (code) {
			case Ti.Geolocation.ERROR_LOCATION_UNKNOWN:
				return "Location unknown";
			case Ti.Geolocation.ERROR_DENIED:
				return "Access denied";
			case Ti.Geolocation.ERROR_NETWORK:
				return "Network error";
			case Ti.Geolocation.ERROR_HEADING_FAILURE:
				return "Failure to detect heading";
			case Ti.Geolocation.ERROR_REGION_MONITORING_DENIED:
				return "Region monitoring access denied";
			case Ti.Geolocation.ERROR_REGION_MONITORING_FAILURE:
				return "Region monitoring access failure";
			case Ti.Geolocation.ERROR_REGION_MONITORING_DELAYED:
				return "Region monitoring setup delayed";
		}
	}
	
	//
	//  SET ACCURACY - THE FOLLOWING VALUES ARE SUPPORTED
	//
	// Titanium.Geolocation.ACCURACY_BEST
	// Titanium.Geolocation.ACCURACY_NEAREST_TEN_METERS
	// Titanium.Geolocation.ACCURACY_HUNDRED_METERS
	// Titanium.Geolocation.ACCURACY_KILOMETER
	// Titanium.Geolocation.ACCURACY_THREE_KILOMETERS
	//
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
	
	//
	//  SET DISTANCE FILTER.  THIS DICTATES HOW OFTEN AN EVENT FIRES BASED ON THE DISTANCE THE DEVICE MOVES
	//  THIS VALUE IS IN METERS
	//
	Titanium.Geolocation.distanceFilter = 10;
	
	Titanium.Geolocation.getCurrentPosition(function(e)
	{
		if (!e.success || e.error)
		{
			// currentLocation.text = 'error: ' + JSON.stringify(e.error);
			Ti.API.info("Code translation: "+translateErrorCode(e.code));
			alert('error ' + JSON.stringify(e.error));
			return;
		}

		var longitude = e.coords.longitude;
		var latitude = e.coords.latitude;
		var altitude = e.coords.altitude;
		var heading = e.coords.heading;
		var accuracy = e.coords.accuracy;
		var speed = e.coords.speed;
		var timestamp = e.coords.timestamp;
		var altitudeAccuracy = e.coords.altitudeAccuracy;
		
		Titanium.API.info('geo - current location: ' + new Date(timestamp) + ' long ' + longitude + ' lat ' + latitude + ' accuracy ' + accuracy);
		// return [latitude,longitude];
		callback(latitude,longitude);
	});
}