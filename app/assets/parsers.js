function readFile(filename) {
	var file = Ti.Filesystem.getFile(filename);
	var blob = file.read();
	var readText = blob.text;
	
	return readText;
}

// returns a [][] array including the header
function parseCSV(text) {
	var rows = text.split("\t\n");
	var data = [];
	_.each(rows,function(row){
		row = row.replace(/"/g, "");
		data.push(row.split("\t"));
	});
	
	return data;
}

Ti.include('location.js');

// take the clinics [][] and save to Globals
function parseClinics(clinics) {
	var clinicsList = [];
	_.each(clinics.slice(1),function(clinic) {
		var split = clinic[24].split(',');
		var coord = {
			latitude:split[1],
			longitude:split[0]
		};
		
		var clinicObj = {
			name_en:	clinic[2],
			name_zh:	clinic[3],
			address_en:	clinic[4],
			address_zh:	clinic[5],
			// easting:	clinic[6],
			// northing:	clinic[8],
			opening:	clinic[10],
			telephone:	clinic[12],
			latitude:	coord.latitude,
			longitude:	coord.longitude	
		};
		clinicsList.push(clinicObj);
	});
	
	Alloy.Globals.clinics = clinicsList;
}

// take the hospitals [][] and save to Globals
function parseHospitals(hospitals) {
	var hospitalsList = [];
	_.each(hospitals.slice(1),function(hospital) {
		var split = hospital[22].split(',');
		var coord = {
			latitude:split[1],
			longitude:split[0]
		};
		var hospitalObj = {
			name_en:	hospital[2],
			name_zh:	hospital[3],
			address_en:	hospital[4],
			address_zh:	hospital[5],
			// easting:	hospital[6],
			// northing:	hospital[8],
			opening:	hospital[10],
			telephone:	hospital[12],
			latitude:	coord.latitude,
			longitude:	coord.longitude	
		};
		hospitalsList.push(hospitalObj);
	});
	
	Alloy.Globals.hospitals = hospitalsList;
}


// take the toilets [][] and save to Globals
function parseToilets(toilets) {
	var toiletList = [];
	_.each(toilets,function(toilet) {
		var coord = toilet[9].split(",");
		var toiletObj = {
			name_en:	toilet[3],
			name_zh:	"",
			address_en: toilet[4],
			latitude:	coord[0],
			longitude:	coord[1]	
		};
		toiletList.push(toiletObj);
	});
	
	Alloy.Globals.toilets = toiletList;
}


function parsePolice(policeStations) {
	var policeList = [];
	_.each(policeStations.slice(1),function(police) {
		var split = police[5].split(',');
		var coord = {
			latitude:split[1],
			longitude:split[0]
		};
		
		var policeObj = {
			name_en:	police[0],
			name_zh:	police[4],
			address_en:	police[3],
			address_zh:	police[3],
			// easting:	police[6],
			// northing:	police[8],
			telephone:	police[1],
			latitude:	coord.latitude,
			longitude:	coord.longitude	
		};
		policeList.push(policeObj);
	});
	
	Alloy.Globals.police = policeList;
}

// take the consulates [][] and save to Globals
function parseConsulates(consulates) {
	var consulateList = [];
	_.each(consulates,function(consulate) {
		var split = consulate[4].split(',');
		var coord = {
			latitude:split[1],
			longitude:split[0]
		};
		var consulateObj = {
			name_en:	consulate[0],
			name_zh:	consulate[2],
			country:	consulate[1],
			address_en:	toTitleCase(consulate[3]),
			address_zh:	toTitleCase(consulate[3]),
			latitude:	coord.latitude,
			longitude:	coord.longitude		
		};
		consulateList.push(consulateObj);
	});
	
	Alloy.Globals.consulates = consulateList;
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() +                                                                txt.substr(1).toLowerCase(); });
}
