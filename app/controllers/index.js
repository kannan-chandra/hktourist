
$.tabgroup.open();

Alloy.Globals.nav = $.tab;

var policeStrip = Ti.UI.createView();
var hospitalStrip = Ti.UI.createView();
var toiletStrip = Ti.UI.createView();
var consulateStrip = Ti.UI.createView();

reload();
setInterval(reload,900000);	// 15 min


function reload(){
	getCurrentLocation(function(latitude,longitude){
		Alloy.Globals.currentLoc = {
			latitude:latitude,
			longitude:longitude
		};
		
		clear();
		add(latitude,longitude);
		
		Appsee.setLocation(latitude,longitude,1,1);
		Appsee.addEvent('location', {'latitude': latitude, 'longitude': longitude});
		log("current location:" + latitude + " " + longitude);
	});
}

function add(latitude,longitude) {
	addPolice(latitude,longitude);
	addHospitals(latitude,longitude);
	addToilets(latitude,longitude);
	addConsulates();
}

function clear() {
	$.strips.remove(policeStrip);
	$.strips.remove(hospitalStrip);
	$.strips.remove(toiletStrip);
	$.strips.remove(consulateStrip);
}

function addHospitals(latitude,longitude){
	var list = Alloy.Globals.clinics.concat(Alloy.Globals.hospitals);
	var locations = sortByDistance(list,latitude,longitude).slice(0,10);
	hospitalStrip = addStrip(locations,"hospital");
}

function addToilets(latitude,longitude){
	var locations = sortByDistance(Alloy.Globals.toilets,latitude,longitude).slice(0,10);
	toiletStrip = addStrip(locations,"toilet");
}

function addPolice(latitude,longitude){
	// log(sortByDistance(Alloy.Globals.police,latitude,longitude));
	var locations = sortByDistance(Alloy.Globals.police,latitude,longitude).slice(0,10);
	policeStrip = addStrip(locations,"police");
}

function addConsulates() {
	consulateStrip = addStrip([{
		name_en:"Select your consulate..."
	}],"consulate");
}

function addStrip(locations,type){
	var strip = Alloy.createController('strip',{
		locations:locations,
		type:type
	}).getView();
	$.strips.add(strip);
	return strip;
}


