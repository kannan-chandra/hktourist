var args = arguments[0] || {};

var location = args.location;
var type = args.type;

Appsee.addEvent('detail opened', location);
Appsee.addEvent('detail opened', {'type':type});


$.lblName.text = location.name_en;
$.lblAddress.text = location.address_en;
var dist = distance(Alloy.Globals.currentLoc.latitude,
					Alloy.Globals.currentLoc.longitude,
					location.latitude,
					location.longitude);
$.lblDistance.text = Number(dist).toFixed(1) + " km";
$.lblOpening.text = location.opening;

if (location.name_zh == "") {
	$.detailView.remove($.btnChinese);
	$.detailView.remove($.lblChinese);
}




function showChinese(e) {
	// Alloy.Globals.nav.open(Alloy.createController('chinese',{
		// location:location,
		// type:type
	// }).getView());	
	Alloy.createController('chinese',{
		location:location,
		type:type
	}).getView().open();
}


switch (type) {
	case "police":
		setupPolice();
		break;
	case "hospital":
		setupHospital();
		break;
	case "toilet":
		setupToilet();
		break;
	case "consulate":
		setupConsulate();
		break;
}

function setupPolice() {
	$.detailWin.title = "Police";
	$.lblName.color = "#0d6a91";
	$.lblAddress.color = "#29afe6";
	$.lblOpening.color = "#29afe6";
	$.lblDistance.color = "#25aae1";
	$.btnChinese.image = "/images/C1. Chinese-Police.png";
}

function setupHospital() {
	$.detailWin.title = "Hospital";
	$.lblName.color = "#9e1d1d";
	$.lblAddress.color = "#e65252";
	$.lblOpening.color = "#e65252";
	$.lblDistance.color = "#e24a4a";
	$.btnChinese.image = "/images/C2. Chinese-Hospital.png";		
}

function setupToilet() {
	$.detailWin.title = "Toilet";
	$.lblName.color = "#dd7200";
	$.lblAddress.color = "#e7a621";
	$.lblOpening.color = "#e7a621";
	$.lblDistance.color = "#fcb116";
	$.btnChinese.image = "/images/C3. Chinese-Toilets.png";	
}

function setupConsulate() {
	$.detailWin.title = "Consulate";
	$.lblName.color = "#428804";
	$.lblAddress.color = "#73c729";
	$.lblOpening.color = "#73c729";
	$.lblDistance.color = "#64b819";
	$.btnChinese.image = "/images/C4. Chinese-Consulate.png";
	$.detailView.remove($.lblOpening);
}



function report(evt) {
    Ti.API.info("Annotation " + evt.title + " clicked, id: " + evt.annotation.myid);
}

// API calls to the map module need to use the Alloy.Globals.Map reference
var locationDot = Alloy.Globals.Map.createAnnotation({
    latitude:	location.latitude,
    longitude:	location.longitude,
    title:		location.name_en,
    subtitle:	"",
    pincolor:Alloy.Globals.Map.ANNOTATION_RED,
    myid:1 // Custom property to uniquely identify this annotation.
});

$.mapview.region = {latitude:location.latitude, longitude:location.longitude,
                    latitudeDelta:0.004, longitudeDelta:0.004};
$.mapview.addAnnotation(locationDot);
$.mapview.height = 200;
