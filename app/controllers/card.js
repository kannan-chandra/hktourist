var args = arguments[0] || {};

$.btnNext.addEventListener('click',args.next);
$.btnPrev.addEventListener('click',args.prev);

if (args.last) {
	$.btnNext.enabled = false;
	$.btnNext.visible = false;
}
if (args.first) {
	$.btnPrev.enabled = false;
	$.btnPrev.visible = false;
}
	

Ti.include('location.js');

var location = args.location;
	
$.lblName.text = location.name_en;
$.lblAddress.text = location.address_en;
var dist = distance(Alloy.Globals.currentLoc.latitude,
					Alloy.Globals.currentLoc.longitude,
					location.latitude,
					location.longitude);
$.lblDistance.text = Number(dist).toFixed(1) + " km";

var type = args.type;
$.lblType.text = type.toUpperCase();

switch (type) {
	case "police":
		setupPolice();
		break;
	case "hospital":
		setupHospital();
		break;
	case "toilet":
		setupToilet();
		break;
	case "consulate":
		setupConsulate();
		break;
}

function setupPolice() {
	$.card.borderColor = $.typeView.backgroundColor = "#25aae1";
	$.lblName.color = "#0d6a91";
	$.lblAddress.color = "#29afe6";
	$.lblDistance.color = "#25aae1";
	$.imgIcon.image = "/images/A1. Police.png";
}

function setupHospital() {
	$.card.borderColor = $.typeView.backgroundColor = "#e24a4a";
	$.lblName.color = "#9e1d1d";
	$.lblAddress.color = "#e65252";
	$.lblDistance.color = "#e24a4a";
	$.imgIcon.image = "/images/A2. Hospital.png";		
}

function setupToilet() {
	$.card.borderColor = $.typeView.backgroundColor = "#fcb116";
	$.lblName.color = "#dd7200";
	$.lblAddress.color = "#e7a621";
	$.lblDistance.color = "#fcb116";
	$.imgIcon.image = "/images/A3. Toilets.png";	
}

function setupConsulate() {
	$.card.borderColor = $.typeView.backgroundColor = "#64b819";
	$.lblName.color = "#428804";
	$.lblAddress.color = "#73c729";
	$.lblDistance.color = "#64b819";
	$.imgIcon.image = "/images/A4. Consulate.png";
	$.lblDistance.visible = false;
}


function click(e) {
	if (type=="consulate"){
		Alloy.Globals.nav.open(Alloy.createController('consulateList').getView());
	} else {
		Alloy.Globals.nav.open(Alloy.createController('detail',{
			location:location,
			type:type
		}).getView());		
	}
}
