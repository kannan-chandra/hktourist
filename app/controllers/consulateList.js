var args = arguments[0] || {};

var data = [];

var consulates = _.sortBy(Alloy.Globals.consulates,
				function(consulate){return consulate.country;});

_.each(consulates,function(consulate){
	data.push(Ti.UI.createTableViewRow({
		title:consulate.country
	}));
});

$.consulateTable.setData(data);

function openConsulate(e) {
	Alloy.Globals.nav.open(Alloy.createController('detail',{
		location:consulates[e.index],
		type:"consulate"
	}).getView());	
}
