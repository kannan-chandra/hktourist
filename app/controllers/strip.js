var args = arguments[0] || {};

Ti.include('utils.js');

var locations = args.locations;
var type = args.type;

function next(){
	log("next!");
	$.strip.scrollToView($.strip.currentPage + 1);
}

function prev(){
	log("prev!");
	$.strip.scrollToView($.strip.currentPage - 1);
}

var views = [];
_.each(locations,function(location,index){
	if (index<=Alloy.Globals.numOfResults) {
		views.push(Alloy.createController('card',{
			first: index==0,
			last:index==(locations.length-1),
			location:location,
			next:next,
			prev:prev,
			type:type
		}).getView());
	}
});
$.strip.views = views;

