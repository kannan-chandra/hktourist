// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.numOfResults = 10;
Alloy.Globals.currentLoc = {
	latitude:22.277587890625,
	longitude:114.17189025878906
};

Alloy.Globals.Map = require('ti.map');

Ti.include('parsers.js');
Ti.include('location.js');
Ti.include('utils.js');

var Appsee = require('com.appsee');
// Appsee.start("42a64be428a84e66aeaff7d57258bf9c");



parseClinics(parseCSV(readFile("data/X CLINIC_HA_20140117 LONGLAT FILTERED reformatted.csv")));
parseHospitals(parseCSV(readFile("data/HOSPITAL_HA_20140117 - LONGLAT FILTERED reformatted.csv")));
parseToilets(parseCSV(readFile("data/toiletmap.csv")));
parsePolice(parseCSV(readFile("data/POlice.csv")));
parseConsulates(parseCSV(readFile("data/Consulate.csv")));

var list = Alloy.Globals.police;
log(_.max(list,function(item){
	return item.address_en.length;
}));
log(_.max(list,function(item){
	return item.address_zh.length;
}));
log(_.max(list,function(item){
	return item.name_zh.length;
}));

// log(Alloy.Globals.consulates);

// var list = Alloy.Globals.consulates;
// log(_.sortBy(list,function(consul){return consul.name_en;}));
